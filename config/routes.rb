Rails.application.routes.draw do
  # get 'articles/index'
  root 'articles#index'
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  resources :articles
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
